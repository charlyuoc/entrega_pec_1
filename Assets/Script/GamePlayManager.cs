﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections; 
using System.Collections.Generic;


public class GamePlayManager : MonoBehaviour
{
    //Instancio/creo un objeto de la clase StoryFiller (llamo al constructor() de esta clase
    // para acceder a todos sus estados y comportamientos
    StoryFiller story = new StoryFiller();

    //Variables para modificar las propiedades text
    //pregunta
    public Text questionShow;
    //posible respuesta
    //public Text answerShow;

    //Variable para modificar las propiedades Transform
    public Transform answersParent;

    //Variable para clonar botones dinamicamente
    public GameObject buttonAnswerPrefab;

    // Use this for initialization
    void Start()
    {
        //Cargo los datos
        story.FillStory();
        questionShow.text = "";
        FillUI();
    }

 
    void FillUI() {
        // Aleatorio
        questionShow.text += "\n\n  " + story.questionText[UnityEngine.Random.Range(0, 3)];
        //answerShow.text = story.answerText1[0];
        questionShow.color = Color.red;
        questionShow.fontStyle = FontStyle.Bold;
        questionShow.fontStyle = FontStyle.Italic;
        //Debug.Log(story.answerText3[1]);


        foreach (Transform child in answersParent.transform)
        {
            Destroy(child.gameObject);
        }
        bool isLeft = true; //variable que me asegura que el primer boton se coloca en la posicion deseada y 
        //los demas debajo (rect.y * 1.3f;) ya que se hace falsa la variable
        float axisY = 50;
        int index = 0;
        // Colocamos los nuevos botones
        // Por cada respuesta crearemos un botón

        foreach (string answer in story.answerText1){
            Debug.Log("respuesta: " + answer);
            // Creamos el botón y lo asociamos al enemento de la UI correspondiente.
            GameObject buttonAnswerCopy = Instantiate(buttonAnswerPrefab);
            buttonAnswerCopy.transform.parent = answersParent;

           
            Debug.Log("localPosition del objeto bACopy inicial: " +buttonAnswerCopy.GetComponent<RectTransform>().localPosition);
            Debug.Log("posicion rect.x inicial: " + buttonAnswerCopy.GetComponent<RectTransform>().rect.x);

            // Le damos la posición adecuada según toque
            float x = buttonAnswerCopy.GetComponent<RectTransform>().rect.x * 0;
            Debug.Log("posicion rect.x final: " +x);

            buttonAnswerCopy.GetComponent<RectTransform>().localPosition = new Vector3(isLeft ? x : -x, axisY, 0);
            Debug.Log("localPosition del objeto bACopy final: " + buttonAnswerCopy.GetComponent<RectTransform>().localPosition);

            // Si es el de la derecha, sumamos distancia para colocarlos en la siguiente línea
            //if (!isLeft)
            //{
                axisY += buttonAnswerCopy.GetComponent<RectTransform>().rect.y * 2.5f;
           //}
            isLeft = !isLeft; //cambio de true a false para que se ejecute la segunda condicion del ternario
            // Asociamos
            // FillListener(buttonAnswerCopy.GetComponent<Button>(), index);

            // Rellenamos el texto del botón
            buttonAnswerCopy.GetComponentInChildren<Text>().text = answer;
            Debug.Log("Texto hijo: " + buttonAnswerCopy.GetComponentInChildren<Text>().text);
            index++;

            Debug.Log("¿esta a la izquierda: "  + isLeft);
        }
        Debug.Log("index: " + index);
    }

   


	
	// Update is called  once per frame
	void Update () {
		
	}
}
