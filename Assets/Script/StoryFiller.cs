﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryFiller 
{
    public string[] questionText;
    public string[] answerText1;
    public string[] answerText2;
    public string[] answerText3;

    public StoryFiller()
    {
        questionText = new string[3]; //no necesita implicitamente una inicializacio
    }

    public void FillStory()
    {
        questionText = new string[]{ "uno", "dos", "tres" };
        answerText1 = new string[4] {"once","doce","trece","catorce"};
        answerText2 = new string[3] { "veintiuno", "veintidos", "veintitres" };
        answerText3 = new string[3] { "treinta y uno", "treinta y dos", "treinta y tres" };
    }
}
